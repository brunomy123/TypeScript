import { NegociacaoController } from './controllers/NegociacaoController';
const controller = new NegociacaoController();


$('.form').submit(controller.adiciona.bind(controller));

$('#botao_importa').click(controller.importaDados.bind(controller));

// document
//     .querySelector('.form')
//     .addEventListener('submit', controller.adiciona.bind(controller));



// Importante para usar no criador de site (classe NegociacaoView) e o cod abaixo
// var view2: NegociacaoView[] = [];
// view2[0] = new NegociacaoView();
// view2[1] = new NegociacaoView();
// var a : any = view2[0].template() + view2[1].template();
// console.log(a);