import { Negociacao } from '../models/Negociacao';
import { Negociacoes } from '../models/Negociacoes';
import { NegociacaoView } from '../views/NegociacoesView';
import { MensagemView } from '../views/MensagemView';
// import { logarTempoDeExecucao } from '../helpers/decorators/logarTempoDeExecucao';

export class NegociacaoController {

    private _inputData: JQuery;
    private _inputQuantidade: JQuery;
    private _inputValor: JQuery;
    private _negociacoes = new Negociacoes();
    private _negociacaoView = new NegociacaoView('#negociacaoView');
    private _mensagemView = new MensagemView('#mensagemView');

    constructor() {

        this._inputData = $('#data');
        this._inputQuantidade = $('#quantidade');
        this._inputValor = $('#valor');
        this._negociacaoView.update(this._negociacoes);
    }


    // @logarTempoDeExecucao()
    adiciona(event: Event) {

        event.preventDefault();


        let data = new Date(this._inputData.val().replace(/-/g, ','));

        if(!this.ehDiaUtil(data)){
            this._mensagemView.update('Somente negociações em dias úteis!');
            return 
        }

        const negociacao = new Negociacao(
            data,
            parseInt(this._inputQuantidade.val()),
            parseFloat(this._inputValor.val())
        );



        this._negociacoes.adiciona(negociacao);

        this._negociacaoView.update(this._negociacoes);
        
        this._mensagemView.update('Negociação Adicionada!');





        // this._negociacaoes.paraArray().forEach(negociacao => {
        //     console.log(negociacao.data);
        //     console.log(negociacao.quantidade);
        //     console.log(negociacao.valor);
        // });
    }

    private ehDiaUtil(data: Date){
        return  data.getDay() != DiaDaSemana.Sabado && data.getDay() != DiaDaSemana.Domingo; 
    }

    importaDados(){

        function isOK(res: Response){

            if(res.ok){
                return res;
            }
            else{
                throw new Error(res.statusText);
            }
        }

        fetch('http://localhost:8080/dados')
            .then(res => isOK(res))
            .then(res => res.json())
            .then((dados: any[]) => {
                dados
                    .map(dado => new Negociacao(new Date(), dado.vezes, dado.montante))
                    .forEach(negociacao => this._negociacoes.adiciona(negociacao));
                this._negociacaoView.update(this._negociacoes);
            })
            .catch(err => console.log(err.message));
    }
}

enum DiaDaSemana {
    Domingo,
    Segunda,
    Terca,
    Quarta,
    Quinta,
    Sexta,
    Sabado
}