import { logarTempoDeExecucao } from '../helpers/decorators/logarTempoDeExecucao';
import { Negociacao } from './Negociacao';

export class Negociacoes {

    private _negociacoes: Negociacao[] = [];  //ou Array<Negociacao>

    adiciona(negociacao: Negociacao) : void{

        this._negociacoes.push(negociacao);
    }

    @logarTempoDeExecucao(true)

    paraArray() : Negociacao[] {
        return ([] as Negociacao[]).concat(this._negociacoes);
    }
}